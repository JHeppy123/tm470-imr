USE imr_database;

/**************************************************
Created by		|	Date		| 	Comment
----------------------------------------------------
J Hepburn		|	14/03/2020	|	Initial version - admins can execute all procedures,
									while engineers and managers can only perform their
                                    necessary functions
J Hepburn		|	23/03/2020	|	Changed the separate revoke statements to a blanket
									'REVOKE ALL' for each role
****************************************************/

/**********************************
Create roles
**********************************/
DROP ROLE IF EXISTS 'engineer'@'localhost';
DROP ROLE IF EXISTS 'manager'@'localhost';
DROP ROLE IF EXISTS 'admin_manager'@'localhost';
CREATE ROLE IF NOT EXISTS 'engineer'@'localhost';
CREATE ROLE IF NOT EXISTS 'manager'@'localhost';
CREATE ROLE IF NOT EXISTS 'admin_manager'@'localhost';

/**********************************
GRANT/REVOKE privileges
**********************************/

-- engineer
REVOKE ALL ON *.* FROM 'engineer'@'localhost';

GRANT EXECUTE ON PROCEDURE engineer_complete_job TO 'engineer'@'localhost';
GRANT EXECUTE ON PROCEDURE add_photograph TO 'engineer'@'localhost';
GRANT EXECUTE ON PROCEDURE add_job_photograph TO 'engineer'@'localhost';
GRANT EXECUTE ON PROCEDURE add_engineer_area TO 'engineer'@'localhost';

-- manager
REVOKE ALL ON *.* FROM 'manager'@'localhost';

GRANT EXECUTE ON PROCEDURE insert_job TO 'manager'@'localhost';
GRANT EXECUTE ON PROCEDURE activate_deactivate_job TO 'manager'@'localhost';
GRANT EXECUTE ON PROCEDURE add_engineer_job TO 'manager'@'localhost';
GRANT EXECUTE ON PROCEDURE management_complete_job TO 'manager'@'localhost';
GRANT EXECUTE ON PROCEDURE add_imr_vehicle TO 'manager'@'localhost';
GRANT EXECUTE ON PROCEDURE add_engineer_vehicle TO 'manager'@'localhost';
GRANT EXECUTE ON PROCEDURE add_imr_area TO 'manager'@'localhost';
GRANT EXECUTE ON PROCEDURE add_engineer_area TO 'manager'@'localhost';
GRANT EXECUTE ON PROCEDURE add_imr_customer TO 'manager'@'localhost';
GRANT EXECUTE ON PROCEDURE remove_engineer_area TO 'manager'@'localhost';
GRANT EXECUTE ON PROCEDURE remove_engineer_job TO 'manager'@'localhost';
GRANT EXECUTE ON PROCEDURE remove_engineer_vehicle TO 'manager'@'localhost';

-- admin_manager
REVOKE ALL ON *.* FROM 'admin_manager'@'localhost';

GRANT EXECUTE ON PROCEDURE insert_job TO 'admin_manager'@'localhost';
GRANT EXECUTE ON PROCEDURE activate_deactivate_job TO 'admin_manager'@'localhost';
GRANT EXECUTE ON PROCEDURE add_engineer_job TO 'admin_manager'@'localhost';
GRANT EXECUTE ON PROCEDURE management_complete_job TO 'admin_manager'@'localhost';
GRANT EXECUTE ON PROCEDURE add_imr_vehicle TO 'admin_manager'@'localhost';
GRANT EXECUTE ON PROCEDURE add_engineer_vehicle TO 'admin_manager'@'localhost';
GRANT EXECUTE ON PROCEDURE add_imr_area TO 'admin_manager'@'localhost';
GRANT EXECUTE ON PROCEDURE add_engineer_area TO 'admin_manager'@'localhost';
GRANT EXECUTE ON PROCEDURE add_imr_customer TO 'admin_manager'@'localhost';
GRANT EXECUTE ON PROCEDURE remove_engineer_area TO 'admin_manager'@'localhost';
GRANT EXECUTE ON PROCEDURE remove_engineer_job TO 'admin_manager'@'localhost';
GRANT EXECUTE ON PROCEDURE remove_engineer_vehicle TO 'admin_manager'@'localhost';
GRANT EXECUTE ON PROCEDURE add_imr_user TO 'admin_manager'@'localhost';
GRANT EXECUTE ON PROCEDURE add_imr_user_type TO 'admin_manager'@'localhost';
GRANT EXECUTE ON PROCEDURE engineer_complete_job TO 'admin_manager'@'localhost';
GRANT EXECUTE ON PROCEDURE add_photograph TO 'admin_manager'@'localhost';
GRANT EXECUTE ON PROCEDURE add_job_photograph TO 'admin_manager'@'localhost';
GRANT EXECUTE ON PROCEDURE add_engineer_area TO 'admin_manager'@'localhost';

