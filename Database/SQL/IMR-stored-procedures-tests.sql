use imr_database;

DELIMITER //

START TRANSACTION;
DROP PROCEDURE IF EXISTS test_inserts_procedure;
COMMIT;
CREATE PROCEDURE test_inserts_procedure ()
BEGIN

	/**************************************************
	Created by		|	Date		| 	Comment
    ----------------------------------------------------
    J Hepburn		|	14/03/2020	|	Initial version; note, requires at least one job_type to be inserted manually
    J Hepburn		|	14/03/2020	|	Update a few parameter values during testing
	****************************************************/
    
	DECLARE new_customer_number INT DEFAULT -1;
	DECLARE new_job_id INT DEFAULT -1;
	DECLARE new_user_type INT DEFAULT -1;
	DECLARE new_eng_user_id INT DEFAULT -1;
	DECLARE new_man_user_id INT DEFAULT -1;
	DECLARE new_imr_area_id INT DEFAULT -1;
	DECLARE new_imr_vehicle_id INT DEFAULT -1;
	DECLARE new_photograph_id INT DEFAULT -1;
	
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
	BEGIN
		ROLLBACK;
        RESIGNAL;
	END;
	START TRANSACTION;
        
		CALL add_imr_customer('Domitius Corbulo', 'South Barn', 'Cams Hall Estate', 'Fareham', 'Hampshire', 'AAAAAAA', '0000', 'james@james.com', new_customer_number);
		
		CALL insert_job(new_customer_number, 1, 666, CURRENT_TIMESTAMP(), 'A note', new_job_id);
        
        CALL add_imr_user_type('Engineer', new_user_type);
        CALL add_imr_user('Marcus', 'Agrippa', new_user_type, new_eng_user_id);
        
        CALL add_imr_user_type('Management', new_user_type);
        CALL add_imr_user('Nero', 'Caesar', new_user_type, new_man_user_id);
        
        CALL add_engineer_job(new_eng_user_id, new_job_id);
        
        CALL add_imr_area('Latium', new_imr_area_id);
        
        CALL add_engineer_area(new_eng_user_id, new_imr_area_id);
        
        CALL add_imr_vehicle('ABCDE', 'Rolls', 'Phantom', new_imr_vehicle_id);
        
        CALL add_engineer_vehicle(new_eng_user_id, new_imr_vehicle_id);
        
        -- CALL add_photograph(new_eng_user_id, NULL, new_photograph_id);
        
       --  CALL add_job_photograph(new_job_id, new_photograph_id);
        
        CALL engineer_complete_job(new_eng_user_id, new_job_id);
        
        CALL management_complete_job(new_man_user_id, new_job_id);
        
        CALL activate_deactivate_job(new_job_id, 0); -- deactivates the job
        
    COMMIT;
END;

-- JH: uncomment when the procedure is no longer needed
-- DROP PROCEDURE IF EXISTS test_procedure;

START TRANSACTION;
DROP PROCEDURE IF EXISTS test_removes_procedure;
COMMIT;
CREATE PROCEDURE test_removes_procedure ()
BEGIN

	/**************************************************
	Created by		|	Date		| 	Comment
    ----------------------------------------------------
    J Hepburn		|	14/03/2020	|	Initial version
	****************************************************/
	
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
	BEGIN
		ROLLBACK;
        RESIGNAL;
	END;
	START TRANSACTION;
        
        CALL remove_engineer_area(1, 1); -- note, change these as necessary
        
        CALL remove_engineer_job(1, 1); -- note, change these as necessary
        
        CALL remove_engineer_vehicle(1, 1); -- note, change these as necessary
        
    COMMIT;
END;

-- JH: uncomment when the procedure is no longer needed
-- DROP PROCEDURE IF EXISTS test_procedure;


//






