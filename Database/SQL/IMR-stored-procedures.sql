USE imr_database;

DELIMITER //


START TRANSACTION;
DROP PROCEDURE IF EXISTS add_imr_user_type;
COMMIT;
CREATE PROCEDURE add_imr_user_type
(
	IN in_description VARCHAR(30),
    OUT out_new_user_type_id INT
)
BEGIN

	/**************************************************
	Created by		|	Date		| 	Comment
    ----------------------------------------------------
    J Hepburn		|	07/03/2020	|	Initial version
    J Hepburn		|	14/03/2020	|	Add RESIGNAL
    J Hepburn		|	14/03/2020	|	Add OUT param
    J Hepburn		|	26/05/2020	|	Add list_engineers
	****************************************************/
    
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
	BEGIN
		ROLLBACK;
        RESIGNAL;
	END;
	START TRANSACTION;
		INSERT INTO imr_user_type
        (
			type_description
        )
        VALUES
        (
			in_description
        );
        
        SET out_new_user_type_id = LAST_INSERT_ID();
    COMMIT;
END;

START TRANSACTION;
DROP PROCEDURE IF EXISTS add_imr_user;
COMMIT;
CREATE PROCEDURE add_imr_user
(
	IN in_user_forename VARCHAR(30),
	IN in_user_surname VARCHAR(30),
	IN in_user_type_id INT,
    OUT out_new_user_id INT
)
BEGIN

	/**************************************************
	Created by		|	Date		| 	Comment
    ----------------------------------------------------
    J Hepburn		|	07/03/2020	|	Initial version
    J Hepburn		|	14/03/2020	|	Add RESIGNAL
    J Hepburn		|	14/03/2020	|	Add OUT param
	****************************************************/
    
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
	BEGIN
		ROLLBACK;
        RESIGNAL;
	END;
	START TRANSACTION;
		INSERT INTO imr_user
        (
			forename,
            surname,
            user_type_id
        )
        VALUES
        (
			in_user_forename,
            in_user_surname,
            in_user_type_id
        );
        
        SET out_new_user_id = LAST_INSERT_ID();
    COMMIT;
END;


START TRANSACTION;
DROP PROCEDURE IF EXISTS add_imr_customer;
COMMIT;
CREATE PROCEDURE add_imr_customer
(
	IN in_contact_name VARCHAR(50),
    IN in_address_line_one VARCHAR(30),
    IN in_address_line_two VARCHAR(30),
    IN in_address_town VARCHAR(30),
    IN in_address_county VARCHAR(30),
    IN in_address_postcode VARCHAR(10),
    IN in_telephone VARCHAR(15),
    IN in_email VARCHAR(30),
    OUT out_new_customer_id INT
)
BEGIN

	/**************************************************
	Created by		|	Date		| 	Comment
    ----------------------------------------------------
    J Hepburn		|	07/03/2020	|	Initial version
    J Hepburn		|	14/03/2020	|	Add RESIGNAL
    J Hepburn		|	14/03/2020	|	Add OUT param
	****************************************************/
    
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
	BEGIN
		ROLLBACK;
        RESIGNAL;
	END;
	START TRANSACTION;
		INSERT INTO customer
        (
			contact_name,
            address_line_one,
            address_line_two,
            address_town,
            address_county,
            address_postcode,
            telephone,
            email
        )
        VALUES
        (
			in_contact_name,
			in_address_line_one,
			in_address_line_two,
			in_address_town,
			in_address_county,
			in_address_postcode,
			in_telephone,
			in_email
        );
        
        SET out_new_customer_id = LAST_INSERT_ID();
    COMMIT;
END;


START TRANSACTION;
DROP PROCEDURE IF EXISTS update_imr_customer;
COMMIT;
CREATE PROCEDURE update_imr_customer
(
	IN in_customer_id INT,
	IN in_contact_name VARCHAR(50),
    IN in_address_line_one VARCHAR(30),
    IN in_address_line_two VARCHAR(30),
    IN in_address_town VARCHAR(30),
    IN in_address_county VARCHAR(30),
    IN in_address_postcode VARCHAR(10),
    IN in_telephone VARCHAR(15),
    IN in_email VARCHAR(30)
)
BEGIN

	/**************************************************
	Created by		|	Date		| 	Comment
    ----------------------------------------------------
    J Hepburn		|	13/06/2020	|	Initial version; found need for this when creating the API ICD
	****************************************************/
    
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
	BEGIN
		ROLLBACK;
        RESIGNAL;
	END;
	START TRANSACTION;
		UPDATE customer SET
			contact_name = in_contact_name,
            address_line_one = in_address_line_one,
            address_line_two = in_address_line_two,
            address_town = in_address_town,
            address_county = in_address_county,
            address_postcode = in_address_postcode,
            telephone = in_telephone,
            email = in_email
		WHERE customer_id = in_customer_id;
    COMMIT;
END;



START TRANSACTION;
DROP PROCEDURE IF EXISTS add_imr_area;
COMMIT;
CREATE PROCEDURE add_imr_area
(
	IN in_description VARCHAR(30),
    OUT out_new_area_id INT
)
BEGIN

	/**************************************************
	Created by		|	Date		| 	Comment
    ----------------------------------------------------
    J Hepburn		|	07/03/2020	|	Initial version
    J Hepburn		|	14/03/2020	|	Add RESIGNAL
    J Hepburn		|	14/03/2020	|	Add OUT param
	****************************************************/
    
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
	BEGIN
		ROLLBACK;
        RESIGNAL;
	END;
	START TRANSACTION;
		INSERT INTO imr_area
        (
			area_description
        )
        VALUES
        (
			in_description
        );
        
        SET out_new_area_id = LAST_INSERT_ID();
    COMMIT;
END;

START TRANSACTION;
DROP PROCEDURE IF EXISTS update_imr_area;
COMMIT;
CREATE PROCEDURE update_imr_area
(
	IN in_area_id INT,
	IN in_description VARCHAR(30)
)
BEGIN

	/**************************************************
	Created by		|	Date		| 	Comment
    ----------------------------------------------------
    J Hepburn		|	23/05/2020	|	Initial version
	****************************************************/
    
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
	BEGIN
		ROLLBACK;
        RESIGNAL;
	END;
	START TRANSACTION;
		UPDATE imr_area
        SET imr_area.area_description = in_description
        WHERE imr_area.area_id = in_area_id;
    COMMIT;
END;


START TRANSACTION;
DROP PROCEDURE IF EXISTS remove_imr_area;
COMMIT;
CREATE PROCEDURE remove_imr_area
(
	IN in_area_id INT,
    OUT out_success INT
)
BEGIN

	/**************************************************
	Created by		|	Date		| 	Comment
    ----------------------------------------------------
    J Hepburn		|	23/05/2020	|	Initial version
	****************************************************/
    
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
	BEGIN
		ROLLBACK;
        RESIGNAL;
	END;
	START TRANSACTION;
		DELETE FROM imr_area WHERE imr_area.area_id = in_area_id;
        SET out_success = 1;
    COMMIT;
END;


START TRANSACTION;
DROP PROCEDURE IF EXISTS add_imr_vehicle;
COMMIT;
CREATE PROCEDURE add_imr_vehicle
(
	IN in_registration VARCHAR(10),
	IN in_make VARCHAR(10),
	IN in_model VARCHAR(10),
    OUT out_vehicle_id INT
)
BEGIN

	/**************************************************
	Created by		|	Date		| 	Comment
    ----------------------------------------------------
    J Hepburn		|	07/03/2020	|	Initial version
    J Hepburn		|	14/03/2020	|	Add RESIGNAL
    J Hepburn		|	14/03/2020	|	Add OUT param
	****************************************************/
    
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
	BEGIN
		ROLLBACK;
        RESIGNAL;
	END;
	START TRANSACTION;
		INSERT INTO vehicle
        (
			vehicle_registration,
            vehicle_make,
            vehicle_model
        )
        VALUES
        (
			in_registration,
            in_make,
            in_model
        );
        
        SET out_vehicle_id = LAST_INSERT_ID();
    COMMIT;
END;


START TRANSACTION;
DROP PROCEDURE IF EXISTS remove_imr_vehicle;
COMMIT;
CREATE PROCEDURE remove_imr_vehicle
(
    IN in_vehicle_id INT
)
BEGIN

	/**************************************************
	Created by		|	Date		| 	Comment
    ----------------------------------------------------
    J Hepburn		|	23/05/2020	|	Initial version
	****************************************************/
    
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
	BEGIN
		ROLLBACK;
        RESIGNAL;
	END;
	START TRANSACTION;
		DELETE FROM vehicle WHERE vehicle_id = in_vehicle_id;
    COMMIT;
END;


START TRANSACTION;
DROP PROCEDURE IF EXISTS insert_job;
COMMIT;
CREATE PROCEDURE insert_job
(
	IN in_customer_id INT,
    IN in_job_type_id INT,
    IN in_creating_user_id INT,
    IN in_start_date DATETIME,
    IN in_notes VARCHAR(250),
    OUT out_job_number INT
)
BEGIN

	/**************************************************
	Created by		|	Date		| 	Comment
    ----------------------------------------------------
    J Hepburn		|	07/03/2020	|	Initial version; no need to take 'engineer_complete'
										and similar - there is a separate SP for finishing a job
    J Hepburn		|	14/03/2020	|	Add RESIGNAL
    J Hepburn		|	14/03/2020	|	Add OUT param.  Change NULL to 0 for the 'complete' fields
	****************************************************/
    
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
	BEGIN
		ROLLBACK;
		RESIGNAL;
	END;
	START TRANSACTION;
		INSERT INTO job
        (
			customer_id,
            job_type_id,
            created_by_user_id,
            start_date,
            notes,
            engineer_complete,
            management_complete
        )
        VALUES
        (
			in_customer_id,
			in_job_type_id,
			in_creating_user_id,
			in_start_date,
			in_notes,
            0,
            0
        );
        
		SET out_job_number := LAST_INSERT_ID();
    COMMIT;
END;

START TRANSACTION;
DROP PROCEDURE IF EXISTS activate_deactivate_job;
COMMIT;
CREATE PROCEDURE activate_deactivate_job
(
	IN in_job_id INT,
    IN in_activate_deactivate_flag INT
)
BEGIN

	/**************************************************
	Created by		|	Date		| 	Comment
    ----------------------------------------------------
    J Hepburn		|	08/03/2020	|	Initial version;
    J Hepburn		|	14/03/2020	|	Add RESIGNAL
	****************************************************/
    
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
	BEGIN
		ROLLBACK;
        RESIGNAL;
	END;
	START TRANSACTION;
		UPDATE job
        SET job_active = in_activate_deactivate_flag
        WHERE job_id = in_job_id;
    COMMIT;
END;

START TRANSACTION;
DROP PROCEDURE IF EXISTS add_engineer_vehicle;
COMMIT;
CREATE PROCEDURE add_engineer_vehicle
(
	IN in_engineer_id INT,
    IN in_vehicle_id INT
)
BEGIN

	/**************************************************
	Created by		|	Date		| 	Comment
    ----------------------------------------------------
    J Hepburn		|	08/03/2020	|	Initial version;
    J Hepburn		|	14/03/2020	|	Add RESIGNAL
	****************************************************/
        
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
	BEGIN
		ROLLBACK;
		RESIGNAL;
	END;
	START TRANSACTION;
		INSERT INTO engineer_vehicle
        (
			engineer_id,
            vehicle_id
        )
        VALUES
        (
			in_engineer_id,
            in_vehicle_id
        );
    COMMIT;
END;

START TRANSACTION;
DROP PROCEDURE IF EXISTS remove_engineer_vehicle;
COMMIT;
CREATE PROCEDURE remove_engineer_vehicle
(
	IN in_engineer_id INT,
    IN in_vehicle_id INT
)
BEGIN

	/**************************************************
	Created by		|	Date		| 	Comment
    ----------------------------------------------------
    J Hepburn		|	08/03/2020	|	Initial version;
    J Hepburn		|	14/03/2020	|	Add RESIGNAL
	****************************************************/
    
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
	BEGIN
		ROLLBACK;
        RESIGNAL;
	END;
	START TRANSACTION;
		DELETE FROM engineer_vehicle
        WHERE engineer_id = in_engineer_id AND vehicle_id = in_vehicle_id;
    COMMIT;
END;

START TRANSACTION;
DROP PROCEDURE IF EXISTS add_engineer_area;
COMMIT;
CREATE PROCEDURE add_engineer_area
(
	IN in_engineer_id INT,
    IN in_area_id INT
)
BEGIN

	/**************************************************
	Created by		|	Date		| 	Comment
    ----------------------------------------------------
    J Hepburn		|	08/03/2020	|	Initial version;
    J Hepburn		|	14/03/2020	|	Add RESIGNAL
	****************************************************/
        
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
	BEGIN
		ROLLBACK;
        RESIGNAL;
	END;
	START TRANSACTION;
		INSERT INTO engineer_area
        (
			engineer_id,
            area_id
        )
        VALUES
        (
			in_engineer_id,
            in_area_id
        );
    COMMIT;
END;

START TRANSACTION;
DROP PROCEDURE IF EXISTS remove_engineer_area;
COMMIT;
CREATE PROCEDURE remove_engineer_area
(
	IN in_engineer_id INT,
    IN in_area_id INT
)
BEGIN

	/**************************************************
	Created by		|	Date		| 	Comment
    ----------------------------------------------------
    J Hepburn		|	08/03/2020	|	Initial version;
    J Hepburn		|	14/03/2020	|	Add RESIGNAL
	****************************************************/
        
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
	BEGIN
		ROLLBACK;
        RESIGNAL;
	END;
	START TRANSACTION;
		DELETE FROM engineer_area
        WHERE engineer_id = in_engineer_id AND area_id = in_area_id;
    COMMIT;
END;

START TRANSACTION;
DROP PROCEDURE IF EXISTS add_photograph;
COMMIT;
CREATE PROCEDURE add_photograph
(
	IN in_user_id INT,
	IN in_photograph_data BLOB,
    OUT out_photograph_id INT
)
BEGIN

	/**************************************************
	Created by		|	Date		| 	Comment
    ----------------------------------------------------
    J Hepburn		|	08/03/2020	|	Initial version;
    J Hepburn		|	14/03/2020	|	Add RESIGNAL
    J Hepburn		|	14/03/2020	|	Add OUT param
	****************************************************/
        
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
	BEGIN
		ROLLBACK;
        RESIGNAL;
	END;
	START TRANSACTION;
		INSERT INTO photograph
        (
			uploaded_by_id,
            uploaded_date,
            photograph_data
        )
        VALUES
        (
			in_user_id,
            CURRENT_TIMESTAMP(),
            in_photograph_data
        );
        
        SET out_photograph_id := LAST_INSERT_ID();
    COMMIT;
END;


START TRANSACTION;
DROP PROCEDURE IF EXISTS remove_job_photograph;
COMMIT;
CREATE PROCEDURE remove_job_photograph
(
	IN in_job_id INT,
	IN in_photograph_id INT
)
BEGIN

	/**************************************************
	Created by		|	Date		| 	Comment
    ----------------------------------------------------
    J Hepburn		|	13/06/2020	|	Initial version;
	****************************************************/
        
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
	BEGIN
		ROLLBACK;
        RESIGNAL;
	END;
	START TRANSACTION;
    
		DELETE FROM job_photograph WHERE job_id = in_job_id AND photograph_id = in_photograph_id;
        
    COMMIT;
END;


START TRANSACTION;
DROP PROCEDURE IF EXISTS add_job_photograph;
COMMIT;
CREATE PROCEDURE add_job_photograph
(
	IN in_job_id INT,
	IN in_photograph_id INT
)
BEGIN

	/**************************************************
	Created by		|	Date		| 	Comment
    ----------------------------------------------------
    J Hepburn		|	08/03/2020	|	Initial version;
    J Hepburn		|	14/03/2020	|	Add RESIGNAL
	****************************************************/
        
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
	BEGIN
		ROLLBACK;
        RESIGNAL;
	END;
	START TRANSACTION;
		INSERT INTO job_photograph
        (
			job_id,
            photograph_id
        )
        VALUES
        (
			in_job_id,
            in_photograph_id
        );
    COMMIT;
END;


START TRANSACTION;
DROP PROCEDURE IF EXISTS add_engineer_job;
COMMIT;
CREATE PROCEDURE add_engineer_job
(
	IN in_engineer_id INT,
	IN in_job_id INT
)
BEGIN

	/**************************************************
	Created by		|	Date		| 	Comment
    ----------------------------------------------------
    J Hepburn		|	08/03/2020	|	Initial version;
    J Hepburn		|	14/03/2020	|	Add RESIGNAL
	****************************************************/
        
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
	BEGIN
		ROLLBACK;
        RESIGNAL;
	END;
	START TRANSACTION;
		INSERT INTO engineer_job
        (
			engineer_id,
            job_id
        )
        VALUES
        (
			in_engineer_id,
            in_job_id
        );
    COMMIT;
END;

START TRANSACTION;
DROP PROCEDURE IF EXISTS remove_engineer_job;
COMMIT;
CREATE PROCEDURE remove_engineer_job
(
	IN in_engineer_id INT,
	IN in_job_id INT
)
BEGIN

	/**************************************************
	Created by		|	Date		| 	Comment
    ----------------------------------------------------
    J Hepburn		|	08/03/2020	|	Initial version; only works for active, non-complete jobs
    J Hepburn		|	14/03/2020	|	Add RESIGNAL
    J Hepburn		|	14/03/2020	|	Change to only working on inactive, incomplete jobs, otherwise an
										active job would have no-one working on it.  Also made sure to check
                                        management completion
	****************************************************/
    
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
	BEGIN
		ROLLBACK;
        RESIGNAL;
	END;
	START TRANSACTION;
        DELETE engineer_job FROM engineer_job
        INNER JOIN job ON job.job_id = engineer_job.job_id AND job.engineer_complete = 0 AND job.job_active = 0 AND job.management_complete = 0
        WHERE engineer_job.job_id = in_job_id AND engineer_job.engineer_id = in_engineer_id;
    COMMIT;
END;

START TRANSACTION;
DROP PROCEDURE IF EXISTS engineer_complete_job;
COMMIT;
CREATE PROCEDURE engineer_complete_job
(
	IN in_engineer_id INT,
	IN in_job_id INT
)
BEGIN

	/**************************************************
	Created by		|	Date		| 	Comment
    ----------------------------------------------------
    J Hepburn		|	14/03/2020	|	Initial version;
    J Hepburn		|	14/03/2020	|	Add RESIGNAL
    J Hepburn		|	14/03/2020	|	Correct engineer_complete_by to engineer_complete_id
	****************************************************/
    
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
	BEGIN
		ROLLBACK;
        RESIGNAL;
	END;
	START TRANSACTION;
        UPDATE job
        INNER JOIN engineer_job ON engineer_job.engineer_id = in_engineer_id AND job.job_id = engineer_job.job_id
        SET job.engineer_complete = 1, job.engineer_complete_date = CURRENT_TIMESTAMP(), job.engineer_complete_id = in_engineer_id
        WHERE job.job_id = in_job_id;
    COMMIT;
END;

START TRANSACTION;
DROP PROCEDURE IF EXISTS management_complete_job;
COMMIT;
CREATE PROCEDURE management_complete_job
(
	IN in_user_id INT,
	IN in_job_id INT
)
BEGIN

	/**************************************************
	Created by		|	Date		| 	Comment
    ----------------------------------------------------
    J Hepburn		|	14/03/2020	|	Initial version;
    J Hepburn		|	14/03/2020	|	Add RESIGNAL
    J Hepburn		|	14/03/2020	|	Correct management_complete_by to management_complete_id
	****************************************************/
    
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
	BEGIN
		ROLLBACK;
        RESIGNAL;
	END;
	START TRANSACTION;
       UPDATE job
        SET job.management_complete = 1, job.management_complete_date = CURRENT_TIMESTAMP(), job.management_complete_id = in_user_id
        WHERE job.job_id = in_job_id;
    COMMIT;
END;


START TRANSACTION;
DROP PROCEDURE IF EXISTS add_photograph;
COMMIT;
CREATE PROCEDURE add_photograph
(
	IN in_filename VARCHAR(100),
	IN in_user_id INT,
    OUT out_photograph_id INT
)
BEGIN

	/**************************************************
	Created by		|	Date		| 	Comment
    ----------------------------------------------------
    J Hepburn		|	23/05/2020	|	Initial version;
	****************************************************/
    
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
	BEGIN
		ROLLBACK;
        RESIGNAL;
	END;
	START TRANSACTION;
		INSERT INTO photograph
        (
			uploaded_by_id,
            photograph_filename
        )
        VALUES
        (
			in_user_id,
            in_filename
		);
        
        SET out_photograph_id := LAST_INSERT_ID();
    COMMIT;
END;


START TRANSACTION;
DROP PROCEDURE IF EXISTS add_competency;
COMMIT;
CREATE PROCEDURE add_competency
(
	IN in_competency_description VARCHAR(50),
    OUT out_competency_id INT
)
BEGIN

	/**************************************************
	Created by		|	Date		| 	Comment
    ----------------------------------------------------
    J Hepburn		|	06/06/2020	|	Initial version;
	****************************************************/
    
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
	BEGIN
		ROLLBACK;
        RESIGNAL;
	END;
	START TRANSACTION;
		INSERT INTO competencies
        (
            competency_description
        )
        VALUES
        (
			in_competency_description
		);
        
        SET out_competency_id := LAST_INSERT_ID();
    COMMIT;
END;

START TRANSACTION;
DROP PROCEDURE IF EXISTS remove_competency;
COMMIT;
CREATE PROCEDURE remove_competency
(
	IN in_competency_id INT
)
BEGIN

	/**************************************************
	Created by		|	Date		| 	Comment
    ----------------------------------------------------
    J Hepburn		|	06/06/2020	|	Initial version;
	****************************************************/
    
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
	BEGIN
		ROLLBACK;
        RESIGNAL;
	END;
	START TRANSACTION;
		
        DELETE FROM competencies WHERE competency_id = in_competency_id;
        
    COMMIT;
END;

START TRANSACTION;
DROP PROCEDURE IF EXISTS add_engineer_competency;
COMMIT;
CREATE PROCEDURE add_engineer_competency
(
	IN in_engineer_id INT,
	IN in_competency_id INT
)
BEGIN

	/**************************************************
	Created by		|	Date		| 	Comment
    ----------------------------------------------------
    J Hepburn		|	06/06/2020	|	Initial version;
	****************************************************/
    
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
	BEGIN
		ROLLBACK;
        RESIGNAL;
	END;
	START TRANSACTION;
		INSERT INTO engineer_competencies
        (
            engineer_id,
            competency_id
        )
        VALUES
        (
			in_engineer_id,
            in_competency_id
		);
    COMMIT;
END;

START TRANSACTION;
DROP PROCEDURE IF EXISTS remove_engineer_competency;
COMMIT;
CREATE PROCEDURE remove_engineer_competency
(
	IN in_engineer_id INT,
	IN in_competency_id INT
)
BEGIN

	/**************************************************
	Created by		|	Date		| 	Comment
    ----------------------------------------------------
    J Hepburn		|	06/06/2020	|	Initial version;
	****************************************************/
    
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
	BEGIN
		ROLLBACK;
        RESIGNAL;
	END;
	START TRANSACTION;
    
		DELETE FROM engineer_competencies WHERE engineer_id = in_engineer_id AND competency_id = in_competency_id;
        
    COMMIT;
END;


//
DELIMITER ;



