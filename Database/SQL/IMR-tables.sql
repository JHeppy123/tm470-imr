USE imr_database;

DROP TABLE IF EXISTS user_status;
DROP TABLE IF EXISTS engineer_job;
DROP TABLE IF EXISTS engineer_vehicle;
DROP TABLE IF EXISTS engineer_area;
DROP TABLE IF EXISTS imr_user;
DROP TABLE IF EXISTS imr_user_type;
DROP TABLE IF EXISTS job_photograph;
DROP TABLE IF EXISTS photograph;
DROP TABLE IF EXISTS job;
DROP TABLE IF EXISTS customer;
DROP TABLE IF EXISTS job_type;
DROP TABLE IF EXISTS vehicle;
DROP TABLE IF EXISTS imr_area;

CREATE TABLE IF NOT EXISTS imr_user_type
(
	user_type_id INT NOT NULL UNIQUE AUTO_INCREMENT,
    type_description VARCHAR(30), -- engineer, management...
    CONSTRAINT imr_user_type_id PRIMARY KEY(user_type_id)
);

INSERT INTO imr_user_type VALUES (1, 'Engineer'), (2, 'Manager');

CREATE TABLE IF NOT EXISTS imr_user
(
	user_id INT NOT NULL UNIQUE AUTO_INCREMENT,
    forename VARCHAR(30),
    surname VARCHAR(30),
    user_type_id INT NOT NULL,
    CONSTRAINT imr_user_user_type_id FOREIGN KEY(user_type_id) REFERENCES imr_user_type(user_type_id)
);

CREATE TABLE IF NOT EXISTS user_status
(
	user_id INT NOT NULL,
    signed_in_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP(),
    signed_out_date DATETIME NULL,
    CONSTRAINT user_status_user_id FOREIGN KEY(user_id) REFERENCES imr_user(user_id)
);

CREATE TABLE IF NOT EXISTS competencies
(
	competency_id INT NOT NULL UNIQUE AUTO_INCREMENT,
    competency_description VARCHAR(50)
);

CREATE TABLE IF NOT EXISTS engineer_competencies
(
	engineer_id INT NOT NULL,
    competency_id INT NOT NULL
);

CREATE TABLE IF NOT EXISTS customer
(
	customer_id INT NOT NULL UNIQUE AUTO_INCREMENT,
    contact_name VARCHAR(50),
    address_line_one VARCHAR(30),
    address_line_two VARCHAR(30),
    address_town VARCHAR(30),
	address_county VARCHAR(30),
    address_postcode VARCHAR(10),
    telephone VARCHAR(15),
    email VARCHAR(30),
    CONSTRAINT customer_customer_id PRIMARY KEY(customer_id)
);

CREATE TABLE IF NOT EXISTS job_type
(
	job_type_id INT NOT NULL UNIQUE AUTO_INCREMENT,
    job_description VARCHAR(30) -- i.e. maintenance, installation, service...
);

CREATE TABLE IF NOT EXISTS job
(
	job_id INT NOT NULL UNIQUE AUTO_INCREMENT,
    customer_id INT NOT NULL,
    job_type_id INT NOT NULL,
    created_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP(),
    created_by_user_id INT NOT NULL,
    job_active INT NOT NULL DEFAULT 1,
    start_date DATETIME NULL,
    notes VARCHAR(250), -- may need to increase
    engineer_complete INT NOT NULL DEFAULT 0,
    engineer_complete_date DATETIME NULL,
    engineer_complete_id INT NULL,
    management_complete INT NOT NULL DEFAULT 0,
    management_complete_date DATETIME NULL,
    management_complete_id INT NULL,
    CONSTRAINT job_job_id PRIMARY KEY(job_id),
    CONSTRAINT job_customer_id FOREIGN KEY(customer_id) REFERENCES customer(customer_id),
    CONSTRAINT job_job_type FOREIGN KEY(job_type_id) REFERENCES job_type(job_type_id)
);

CREATE TABLE IF NOT EXISTS photograph
(
	photograph_id INT NOT NULL UNIQUE AUTO_INCREMENT,
    uploaded_by_id INT NOT NULL,
    uploaded_date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP(),
    photograph_filename VARCHAR(100),
    photograph_data BLOB
);

CREATE TABLE IF NOT EXISTS job_photograph
(
	job_id INT NOT NULL,
    photograph_id INT NOT NULL,
    CONSTRAINT job_photograph_job_id FOREIGN KEY(job_id) REFERENCES job(job_id),
    CONSTRAINT job_photograph_photograph_id FOREIGN KEY(photograph_id) REFERENCES photograph(photograph_id)
);

CREATE TABLE IF NOT EXISTS engineer_job
(
	engineer_id INT NOT NULL,
    job_id INT NOT NULL,
    CONSTRAINT engineer_job_engineer_id FOREIGN KEY(engineer_id) REFERENCES imr_user(user_id),
    CONSTRAINT engineer_job_job_id FOREIGN KEY(job_id) REFERENCES job(job_id)
);

CREATE TABLE IF NOT EXISTS vehicle
(
	vehicle_id INT NOT NULL UNIQUE AUTO_INCREMENT,
    vehicle_registration VARCHAR(10),
    vehicle_make VARCHAR(10),
    vehicle_model VARCHAR(10),
    CONSTRAINT vehicle_vehicle_id PRIMARY KEY(vehicle_id)
);

CREATE TABLE IF NOT EXISTS engineer_vehicle
(
	engineer_id INT NOT NULL,
    vehicle_id INT NOT NULL,
    CONSTRAINT engineer_vehicle_engineer_id FOREIGN KEY(engineer_id) REFERENCES imr_user(user_id),
    CONSTRAINT engineer_vehicle_vehicle_id FOREIGN KEY(vehicle_id) REFERENCES vehicle(vehicle_id)
);

CREATE TABLE IF NOT EXISTS imr_area
(
	area_id INT NOT NULL UNIQUE AUTO_INCREMENT,
    area_description VARCHAR(30), -- e.g. Midlands, South East, Hampshire...
    CONSTRAINT area_area_id PRIMARY KEY(area_id)
);

CREATE TABLE IF NOT EXISTS engineer_area
(
	engineer_id INT NOT NULL,
    area_id INT NOT NULL,
    CONSTRAINT engineer_area_engineer_id FOREIGN KEY(engineer_id) REFERENCES imr_user(user_id),
    CONSTRAINT engineer_area_area_id FOREIGN KEY(area_id) REFERENCES imr_area(area_id)
);
