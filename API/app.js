const express = require('express');
const bodyParser = require('body-parser');
const app = express();

const createEngineer = require('./api/routes/user/engineer/create');
const listEngineers = require('./api/routes/user/engineer/list');
const createManager = require('./api/routes/user/manager/create');
const listManagers = require('./api/routes/user/manager/list');

const listJobs = require('./api/routes/job/list');
const listActiveJobs = require('./api/routes/job/listActive');
const listInactiveJobs = require('./api/routes/job/listInactive');
const createJob = require('./api/routes/job/create');
const jobDetails = require('./api/routes/job/details');
const activateJob = require('./api/routes/job/activate');
const deactivateJob = require('./api/routes/job/deactivate');
const assignEngineerJob = require('./api/routes/job/engineer/assign');
const removeEngineerJob = require('./api/routes/job/engineer/remove');
const completeEngineerJob = require('./api/routes/job/engineer/complete');
const completeManagerJob = require('./api/routes/job/manager/complete');
const uploadPhotograph = require('./api/routes/job/photograph/upload');
const assignPhotograph = require('./api/routes/job/photograph/assign');
const removePhotograph = require('./api/routes/job/photograph/remove');

const listAreas = require('./api/routes/area/list');
const createArea = require('./api/routes/area/create');
const updateArea = require('./api/routes/area/update');
const removeArea = require('./api/routes/area/remove');
const assignEngineerArea = require('./api/routes/area/engineer/assign');
const removeEngineerArea = require('./api/routes/area/engineer/remove');

const listVehicles = require('./api/routes/vehicle/list');
const createVehicle = require('./api/routes/vehicle/create');
const removeVehicle = require('./api/routes/vehicle/remove');
const assignEngineerVehicle = require('./api/routes/vehicle/engineer/assign');
const removeEngineerVehicle = require('./api/routes/vehicle/engineer/remove');

const createCompetency = require('./api/routes/user/engineer/competencies/create');
const removeCompetency = require('./api/routes/user/engineer/competencies/remove');
const assignEngineerCompetency = require('./api/routes/user/engineer/assignCompetency');
const removeEngineerCompetency = require('./api/routes/user/engineer/removeCompetency');

const listCustomers = require('./api/routes/customer/list');
const createCustomer = require('./api/routes/customer/create');
const updateCustomer = require('./api/routes/customer/update');

app.use(bodyParser.json());
app.use('/user/engineer/create', createEngineer);
app.use('/user/engineer/list', listEngineers);
app.use('/user/manager/create', createManager);
app.use('/user/manager/list', listManagers);

app.use('/job/list', listJobs);
app.use('/job/listActive', listActiveJobs);
app.use('/job/listInactive', listInactiveJobs);
app.use('/job/create', createJob);
app.use('/job/details', jobDetails);
app.use('/job/activate', activateJob);
app.use('/job/deactivate', deactivateJob);
app.use('/job/engineer/assign', assignEngineerJob);
app.use('/job/engineer/remove', removeEngineerJob);
app.use('/job/engineer/complete', completeEngineerJob);
app.use('/job/manager/complete', completeManagerJob);
app.use('/job/photograph/upload', uploadPhotograph);
app.use('/job/photograph/assign', assignPhotograph);
app.use('/job/photograph/remove', removePhotograph);

app.use('/area/list', listAreas);
app.use('/area/create', createArea);
app.use('/area/update', updateArea);
app.use('/area/remove', removeArea);
app.use('/area/engineer/assign', assignEngineerArea);
app.use('/area/engineer/remove', removeEngineerArea);

app.use('/vehicle/list', listVehicles);
app.use('/vehicle/create', createVehicle);
app.use('/vehicle/remove', removeVehicle);
app.use('/vehicle/engineer/assign', assignEngineerVehicle);
app.use('/vehicle/engineer/remove', removeEngineerVehicle);

app.use('/user/engineer/competencies/create', createCompetency);
app.use('/user/engineer/competencies/remove', removeCompetency);
app.use('/user/engineer/assignCompetency', assignEngineerCompetency);
app.use('/user/engineer/removeCompetency', removeEngineerCompetency);

app.use('/customer/list', listCustomers);
app.use('/customer/create', createCustomer);
app.use('/customer/update', updateCustomer);

module.exports = app;