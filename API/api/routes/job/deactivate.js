const express = require('express');
const router = express.Router();
const multer = require('multer');
const mysqlx = require('../database');

router.post('/', (req, res, next) =>
    {
        mysqlx
        .getSession('root:AdventumCaesaris@localhost')
        .then(function (s) {
          session = s;
      
          return session.getSchema('imr_database');
        })
        .then(function () {
          return Promise.all([
            session.sql('USE imr_database;').execute(),
            session.executeSql('SET @job_id = ?;', req.body.jobId).execute(),
            session.executeSql('SET @activate_flag = 0;').execute(),
            session.sql("CALL activate_deactivate_job(@job_id, @activate_flag);").execute()
          ])
        })
        .then(function () {
            res.status(200).json(
                {
                  "success": true
                }
            );
        });
    }
);

module.exports = router;
