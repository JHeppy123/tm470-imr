const express = require('express');
const router = express.Router();
const multer = require('multer');
const mysqlx = require('../../database');

router.post('/', (req, res, next) =>
    {
        mysqlx
        .getSession('root:AdventumCaesaris@localhost')
        .then(function (s) {
          session = s;
      
          return session.getSchema('imr_database');
        })
        .then(function () {
          return Promise.all([
            session.sql('USE imr_database;').execute(),
            session.executeSql('SET @manager_id = ?;', req.body.manageId).execute(),
            session.executeSql('SET @job_id = ?;', req.body.jobId).execute(),
            session.sql("CALL management_complete_job(@manager_id, @job_id);").execute()
          ])
        })
        .then(function () {
            res.status(200).json(
                {
                  "success": true
                }
            );
        });
    }
);

module.exports = router;
