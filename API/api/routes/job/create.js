const express = require('express');
const router = express.Router();
const multer = require('multer');
const mysqlx = require('../database');

router.post('/', (req, res, next) =>
    {
        var jobId = 0;

        mysqlx
        .getSession('root:AdventumCaesaris@localhost')
        .then(function (s) {
          session = s;
      
          return session.getSchema('imr_database');
        })
        .then(function () {
          return Promise.all([
            session.sql('USE imr_database;').execute(),
            session.executeSql('SET @customer_id = ?;', req.body.customerId).execute(),
            session.executeSql('SET @job_type_id = ?;', req.body.jobTypeId).execute(),
            session.executeSql('SET @creating_user_id = ?;', req.body.creatingUserId).execute(),
            session.executeSql('SET @start_date = ?;', req.body.startDate).execute(),
            session.executeSql('SET @notes = ?;', req.body.notes).execute(),
            session.executeSql('SET @job_number = 0;').execute(),
            session.sql("CALL insert_job(@customer_id, @job_type_id, @creating_user_id, @start_date, @notes, @job_number);").execute(),
            session.sql('SELECT @job_number').execute(function (row) {
                jobId = row
            })
          ])
        })
        .then(function () {
            res.status(200).json(
                {
                    "jobId": jobId.toString()
                }
            );
        });
    }
);

module.exports = router;
