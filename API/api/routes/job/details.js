const express = require('express');
const router = express.Router();
const multer = require('multer');
const mysqlx = require('../database');

router.get('/', (req, res, next) =>
    {
        var jobs = [];

        const requestedJob =
        {
            jobId: req.body.jobId,
            customerName: "",
            createdDate: "",
            createdBy: "",
            active: 0,
            startDate: "",
            engineerComplete: 0,
            managerComplete: 0,
            notes: "",
            assignee: ""
        }

        mysqlx
        .getSession('root:AdventumCaesaris@localhost')
        .then(function (s) {
          session = s;
      
          return session.getSchema('imr_database');
        })
        .then(function () {
          return Promise.all([
            session.sql('USE imr_database;').execute(),
            session.executeSql('SET @jobid = ?;', requestedJob.jobId).execute(),
            session.sql('SELECT job.job_id, customer.contact_name, job.created_date, CONCAT(imr_user.forename, \' \', imr_user.surname) AS \'created_by\', \
                        job.job_active AS \'active\', job.start_date, job.engineer_complete, job.management_complete, job.notes, engineer.engineer FROM job \
                        INNER JOIN customer ON customer.customer_id = job.customer_id \
                        INNER JOIN job_type ON job_type.job_type_id = job.job_type_id \
                        INNER JOIN imr_user ON imr_user.user_id = job.created_by_user_id \
                        LEFT OUTER JOIN (SELECT engineer_job.job_id, CONCAT(imr_user.forename, \' \', imr_user.surname) AS engineer FROM engineer_job \
                        LEFT OUTER JOIN imr_user on imr_user.user_id = engineer_job.engineer_id \
                        INNER JOIN job ON job.job_id = engineer_job.job_id \
                        WHERE engineer_job.job_id = job.job_id) AS engineer ON engineer.job_id = job.job_id \
                        WHERE job.job_id = @jobid;'
                        ).execute(function (row) {
                            requestedJob.jobId = row[0];
                            requestedJob.customerName = row[1];
                            requestedJob.createdDate = row[2];
                            requestedJob.createdBy = row[3];
                            requestedJob.active = row[4];
                            requestedJob.startDate = row[5];
                            requestedJob.engineerComplete = row[6];
                            requestedJob.managerComplete = row[7];
                            requestedJob.notes = row[8];
                            requestedJob.assignee = row[9];
                            }
                        )
        ])
        })
        .then(function () {
            res.status(200).json(
                {
                    "jobId": requestedJob.jobId.toString(),
                    "customerName": requestedJob.customerName,
                    "createdDate": requestedJob.createdDate,
                    "createdBy": requestedJob.createdBy,
                    "active": requestedJob.active == 1 ? true : false,
                    "startDate": requestedJob.startDate,
                    "engineerComplete": requestedJob.engineerComplete == 1 ? true : false,
                    "managerComplete": requestedJob.managerComplete == 1 ? true : false,
                    "notes": requestedJob.notes,
                    "assignee": requestedJob.assignee,
                }
            );
        });
    }
);

module.exports = router;
