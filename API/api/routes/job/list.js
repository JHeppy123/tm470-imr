const express = require('express');
const router = express.Router();
const multer = require('multer');
const mysqlx = require('../database');

router.get('/', (req, res, next) =>
    {
        var jobs = [];

        mysqlx
        .getSession('root:AdventumCaesaris@localhost')
        .then(function (s) {
          session = s;
      
          return session.getSchema('imr_database');
        })
        .then(function () {
          return Promise.all([
            session.sql('USE imr_database;').execute(),
            session.sql('SELECT job.job_id, customer.contact_name, job.created_date, CONCAT(imr_user.forename, \' \', imr_user.surname) AS \'created_by\', \
                        job.job_active AS \'active\', job.start_date, job.engineer_complete, job.management_complete FROM job \
                        INNER JOIN customer ON customer.customer_id = job.customer_id INNER JOIN job_type ON job_type.job_type_id = job.job_type_id \
                        INNER JOIN imr_user ON imr_user.user_id = job.created_by_user_id'
                        ).execute(function (row) {
                            jobs.push(
                                {
                                    jobId: row[0],
                                    customerName: row[1],
                                    createdDate: row[2],
                                    createdBy: row[3],
                                    active: row[4] == 1 ? true : false,
                                    startDate: row[5],
                                    engineerComplete: row[6] == 1 ? true : false,
                                    managerComplete: row[7] == 1 ? true : false
                                }
                            )
                        })
        ])
        })
        .then(function () {
            res.status(200).json(
                jobs
            );
        });
    }
);

module.exports = router;
