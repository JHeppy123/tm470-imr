const express = require('express');
const router = express.Router();
const multer = require('multer');
const mysqlx = require('../../database');

var photograph_id = 0;
var photograph_filename = '';

const imageStorage = multer.diskStorage(
    {
        destination: function(req, file, cb)
        {
            cb(null, './uploads/');
        },
        filename: function(req, file, cb)
        {
            var date = new Date();
            var month = date.getMonth() + 1;
            var date = new Date();
            var month = date.getMonth() + 1;
            //NOTE: Hardcoded to jpgs for now
            photograph_filename = 'imr-photo-' + date.getUTCDate().toString() + '-' + month.toString() + '-' + date.getUTCFullYear().toString()
            + '-' + date.getHours().toString() + '-' + date.getMinutes().toString() + '-' + date.getSeconds().toString() + '.jpg';

            cb(null, photograph_filename);
        }
    }
);
const uploader = multer({storage: imageStorage});

router.get('/',
    (req, res, next) =>
    {
        res.status(200).json({message: "It works!"});
    }
);

//NOTE: Image sent in the POST message must have a key of 'jobimage' in order for this to work
router.post('/', uploader.single('jobimage'), (req, res, next) =>
    {
        //save the filename to the database
        mysqlx
        .getSession('root:AdventumCaesaris@localhost')
        .then(function (s) {
          session = s;
      
          return session.getSchema('imr_database');
        })
        .then(function () {
          return Promise.all([
            session.sql('USE imr_database;').execute(),
            session.executeSql('SET @filename = ?;', photograph_filename).execute(),
            session.executeSql('SET @user_id = ?;', req.body.engineerId).execute(),
            session.executeSql('SET @photograph_id = 0;').execute(),
            session.sql("CALL add_photograph(@filename, @user_id, @photograph_id);").execute(),
            session.sql('SELECT @photograph_id').execute(function (row) {
                photograph_id = row
            })
          ])
        })
        .then(function () {
            res.status(200).json(
                {
                    "photographId": photograph_id.toString()
                }
            );
        });
    }
);

module.exports = router;
