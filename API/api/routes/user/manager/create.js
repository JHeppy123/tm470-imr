const express = require('express');
const router = express.Router();
const multer = require('multer');
const mysqlx = require('../../database');

router.post('/', (req, res, next) =>
    {
        var userId = -1;

        mysqlx
        .getSession('root:AdventumCaesaris@localhost')
        .then(function (s) {
          session = s;
      
          return session.getSchema('imr_database');
        })
        .then(function () {
          return Promise.all([
            session.sql('USE imr_database;').execute(),
            session.executeSql('SET @forename = ?;', req.body.forename).execute(),
            session.executeSql('SET @surname = ?;', req.body.surname).execute(),
            session.executeSql('SET @type = 2;').execute(),
            session.executeSql('SET @new_user_id = 0;').execute(),
            session.sql("CALL add_imr_user(@forename, @surname, @type, @new_user_id);").execute(),
            session.sql('SELECT @new_user_id').execute(function (row) {
                userId = row
            })
          ])
        })
        .then(function () {
            res.status(200).json(
                {
                    "managerId": userId.toString()
                }
            );
        });
    }
);

module.exports = router;
