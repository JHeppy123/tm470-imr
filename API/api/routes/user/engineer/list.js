const express = require('express');
const router = express.Router();
const multer = require('multer');
const mysqlx = require('../../database');

router.get('/', (req, res, next) =>
    {
        const user =
        {
            engineerId: 0,
            forename: req.body.forename,
            surname: req.body.surname
        }

        var engineers = [];

        mysqlx
        .getSession('root:AdventumCaesaris@localhost')
        .then(function (s) {
          session = s;
      
          return session.getSchema('imr_database');
        })
        .then(function () {
          return Promise.all([
            session.sql('USE imr_database;').execute(),
            session.sql('SELECT user_id, forename, surname FROM imr_user WHERE user_type_id = 1;').execute(function (row) {
                engineers.push(
                    {
                        engineerId: row[0],
                        forename: row[1],
                        surname: row[2]
                    }
                )
            })
        ])
        })
        .then(function () {
            res.status(200).json(
                engineers
            );
        });
    }
);

module.exports = router;
