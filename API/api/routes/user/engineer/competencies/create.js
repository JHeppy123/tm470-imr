const express = require('express');
const router = express.Router();
const multer = require('multer');
const mysqlx = require('../../../database');

router.post('/', (req, res, next) =>
    {
        var competencyId = 0;

        mysqlx
        .getSession('root:AdventumCaesaris@localhost')
        .then(function (s) {
          session = s;
      
          return session.getSchema('imr_database');
        })
        .then(function () {
          return Promise.all([
            session.sql('USE imr_database;').execute(),
            session.executeSql('SET @competency_description = ?;', req.body.description).execute(),
            session.executeSql('SET @competency_id = 0;').execute(),
            session.sql("CALL add_competency(@competency_description, @competency_id);").execute(),
            session.sql('SELECT @competency_id').execute(function (row) {
                competencyId = row
            })
          ])
        })
        .then(function () {
            res.status(200).json(
                {
                    "competencyId": competencyId.toString()
                }
            );
        });
    }
);

module.exports = router;
