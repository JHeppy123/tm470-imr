const express = require('express');
const router = express.Router();
const multer = require('multer');
const mysqlx = require('../../database');

router.post('/', (req, res, next) =>
    {
        mysqlx
        .getSession('root:AdventumCaesaris@localhost')
        .then(function (s) {
          session = s;
      
          return session.getSchema('imr_database');
        })
        .then(function () {
          return Promise.all([
            session.sql('USE imr_database;').execute(),
            session.executeSql('SET @engineer_id = ?;', req.body.engineerId).execute(),
            session.executeSql('SET @competency_id = ?;', req.body.competencyId).execute(),
            session.sql("CALL remove_engineer_competency(@engineer_id, @competency_id);").execute()
          ])
        })
        .then(function () {
            res.status(200).json(
                {
                    "success": true
                }
            );
        });
    }
);

module.exports = router;
