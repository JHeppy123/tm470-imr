const express = require('express');
const router = express.Router();
const multer = require('multer');
const mysqlx = require('../database');

router.post('/', (req, res, next) =>
    {
        var vehicleId = -1;

        mysqlx
        .getSession('root:AdventumCaesaris@localhost')
        .then(function (s) {
          session = s;
      
          return session.getSchema('imr_database');
        })
        .then(function () {
          return Promise.all([
            session.sql('USE imr_database;').execute(),
            session.executeSql('SET @vehicle_registration = ?;', req.body.vehicleRegistration).execute(),
            session.executeSql('SET @vehicle_make = ?;', req.body.vehicleMake).execute(),
            session.executeSql('SET @vehicle_model = ?;', req.body.vehicleModel).execute(),
            session.executeSql('SET @new_vehicle_id = 0;').execute(),
            session.sql("CALL add_imr_vehicle(@vehicle_registration, @vehicle_make, @vehicle_model, @new_vehicle_id);").execute(),
            session.sql('SELECT @new_vehicle_id;').execute(function (row) {
                vehcileId = row;
            })
          ])
        })
        .then(function () {
            res.status(200).json(
                {
                    "vehicleId": vehicleId.toString()
                }
            );
        });
    }
);

module.exports = router;
