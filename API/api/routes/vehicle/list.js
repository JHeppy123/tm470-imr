const express = require('express');
const router = express.Router();
const multer = require('multer');
const mysqlx = require('../database');

router.get('/', (req, res, next) =>
    {
        var vehicles = [];

        mysqlx
        .getSession('root:AdventumCaesaris@localhost')
        .then(function (s) {
          session = s;
      
          return session.getSchema('imr_database');
        })
        .then(function () {
          return Promise.all([
            session.sql('USE imr_database;').execute(),
            session.sql('SELECT vehicle.vehicle_id, vehicle.vehicle_registration, vehicle.vehicle_make, vehicle.vehicle_model, CONCAT(imr_user.forename, \' \', imr_user.surname) AS \'assignee\' \
                         FROM vehicle \
                         LEFT OUTER JOIN engineer_vehicle ON engineer_vehicle.vehicle_id = vehicle.vehicle_id \
                         LEFT OUTER JOIN imr_user ON imr_user.user_id = engineer_vehicle.engineer_id;').execute(function (row) {
                vehicles.push(
                    {
                        vehicleId: row[0],
                        vehicleRegistration: row[1],
                        vehicleMake: row[2],
                        vehicleModel: row[3],
                        engineer: row[4]
                    }
                )
            })
        ])
        })
        .then(function () {
            res.status(200).json(
                vehicles
            );
        });
    }
);

module.exports = router;
