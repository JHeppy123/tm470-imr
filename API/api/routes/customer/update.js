const express = require('express');
const router = express.Router();
const multer = require('multer');
const mysqlx = require('../database');

router.post('/', (req, res, next) =>
    {
        mysqlx
        .getSession('root:AdventumCaesaris@localhost')
        .then(function (s) {
          session = s;
      
          return session.getSchema('imr_database');
        })
        .then(function () {
          return Promise.all([
            session.sql('USE imr_database;').execute(),
            session.executeSql('SET @customer_id = ?;', req.body.customerId).execute(),
            session.executeSql('SET @customer_name = ?;', req.body.name).execute(),
            session.executeSql('SET @address_one = ?;', req.body.addressOne).execute(),
            session.executeSql('SET @address_two = ?;', req.body.addressTwo).execute(),
            session.executeSql('SET @address_town = ?;', req.body.addressTown).execute(),
            session.executeSql('SET @address_county = ?;', req.body.addressCounty).execute(),
            session.executeSql('SET @address_postcode = ?;', req.body.addressPostcode).execute(),
            session.executeSql('SET @telephone = ?;', req.body.telephone).execute(),
            session.executeSql('SET @email = ?;', req.body.email).execute(),
            session.executeSql('SET @new_customer_id = -1;').execute(),
            session.sql("CALL update_imr_customer(@customer_id, @customer_name, @address_one, @address_two, @address_town, @address_county, @address_postcode, @telephone, @email);").execute(),
            //need some sort of check here to make sure it worked
          ])
        })
        .then(function () {
            res.status(200).json(
                {
                    "success": true
                }
            );
        });
    }
);

module.exports = router;
