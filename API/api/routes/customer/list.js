const express = require('express');
const router = express.Router();
const multer = require('multer');
const mysqlx = require('../database');

router.get('/', (req, res, next) =>
    {
        var customers = [];

        mysqlx
        .getSession('root:AdventumCaesaris@localhost')
        .then(function (s) {
          session = s;
      
          return session.getSchema('imr_database');
        })
        .then(function () {
          return Promise.all([
            session.sql('USE imr_database;').execute(),
            session.sql('SELECT customer_id, contact_name, address_line_one, address_line_two, address_town, address_county, address_postcode, telephone, email \
                         FROM customer;').execute(function (row) {
                    customers.push(
                    {
                        "customerId": row[0].toString(),
                        "name": row[1],
                        "addressOne": row[2],
                        "addressTwo": row[3],
                        "addressTown": row[4],
                        "addressCounty": row[5],
                        "addressPostcode": row[6],
                        "telephone": row[7],
                        "email": row[8]
                    }
                )
            })
        ])
        })
        .then(function () {
            res.status(200).json(
                customers
            );
        });
    }
);

module.exports = router;
