const express = require('express');
const router = express.Router();
const multer = require('multer');
const mysqlx = require('../database');

router.get('/', (req, res, next) =>
    {
        const area =
        {
            areaId: 0,
            areaDescription: ""
        }

        var areas = [];

        mysqlx
        .getSession('root:AdventumCaesaris@localhost')
        .then(function (s) {
          session = s;
      
          return session.getSchema('imr_database');
        })
        .then(function () {
          return Promise.all([
            session.sql('USE imr_database;').execute(),
            session.sql('SELECT area_id, area_description FROM imr_area;').execute(function (row) {
                areas.push(
                    {
                        areaId: row[0],
                        areaDescription: row[1]
                    }
                )
            })
        ])
        })
        .then(function () {
            res.status(200).json(
                areas
            );
        });
    }
);

module.exports = router;
