const express = require('express');
const router = express.Router();
const multer = require('multer');
const mysqlx = require('../database');

router.post('/', (req, res, next) =>
    {
        var success = false;

        mysqlx
        .getSession('root:AdventumCaesaris@localhost')
        .then(function (s) {
          session = s;
      
          return session.getSchema('imr_database');
        })
        .then(function () {
          return Promise.all([
            session.sql('USE imr_database;').execute(),
            session.executeSql('SET @area_id = ?;', req.body.areaId).execute(),
            session.executeSql('SET @success = 0;').execute(),
            session.sql("CALL remove_imr_area(@area_id, @success);").execute(),
            session.sql('SELECT @success').execute(function (row) {
                if (row[0] == 1)
                {
                    success = true
                }
            })
          ])
        })
        .then(function () {
            if (success)
            {
                res.status(200).json(
                    {
                        "success": true
                    }
                );
            }
            else
            {
                res.status(200).json(
                    {
                        "success": false
                    }
                );
            }
        });
    }
);

module.exports = router;
