const express = require('express');
const router = express.Router();
const multer = require('multer');
const mysqlx = require('../database');

router.post('/', (req, res, next) =>
    {
        const area =
        {
            areaId: 0,
            areaDescription: req.body.areaDescription
        }

        mysqlx
        .getSession('root:AdventumCaesaris@localhost')
        .then(function (s) {
          session = s;
      
          return session.getSchema('imr_database');
        })
        .then(function () {
          return Promise.all([
            session.sql('USE imr_database;').execute(),
            session.executeSql('SET @area_description = ?;', area.areaDescription).execute(),
            session.executeSql('SET @area_id = 0;').execute(),
            session.sql("CALL add_imr_area(@area_description, @area_id);").execute(),
            session.sql('SELECT @area_id').execute(function (row) {
                area.areaId = row
            })
          ])
        })
        .then(function () {
            res.status(200).json(
                {
                    "areaId": area.areaId.toString()
                }
            );
        });
    }
);

module.exports = router;
