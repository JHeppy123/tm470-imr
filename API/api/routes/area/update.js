const express = require('express');
const router = express.Router();
const multer = require('multer');
const mysqlx = require('../database');

router.post('/', (req, res, next) =>
    {
        mysqlx
        .getSession('root:AdventumCaesaris@localhost')
        .then(function (s) {
          session = s;
      
          return session.getSchema('imr_database');
        })
        .then(function () {
          return Promise.all([
            session.sql('USE imr_database;').execute(),
            session.executeSql('SET @area_id = ?;', req.body.areaId).execute(),
            session.executeSql('SET @area_description = ?;', req.body.areaDescription).execute(),
            session.sql("CALL update_imr_area(@area_id, @area_description);").execute()
          ])
        })
        .then(function () {
            res.status(200).json(
              {
                "success": true
              }
            );
        });
    }
);

module.exports = router;
