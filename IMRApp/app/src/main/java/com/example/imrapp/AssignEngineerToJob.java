package com.example.imrapp;

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.StringTokenizer;

public class AssignEngineerToJob extends AppCompatActivity {

    private RecyclerView engineerListView;
    private IMRRecycleViewAdapter engineerListAdapter;
    private RecyclerView.LayoutManager engineerListLayoutManager;

    String selectedEngineerId;
    String selectedJobId;

    Button assignEngineerButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assign_engineer_to_job);

        selectedJobId = getIntent().getStringExtra("selectedJobId");

        if (selectedJobId.isEmpty()) {
            this.onStop();
        }

        assignEngineerButton = (Button)findViewById(R.id.assignEngineerButton);
        assignEngineerButton.setEnabled(false);

        engineerListView = (RecyclerView) findViewById(R.id.assignEngineerEngineerList);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        engineerListView.setHasFixedSize(true);
        // use a linear layout manager
        engineerListLayoutManager = new LinearLayoutManager(this);
        engineerListView.setLayoutManager(engineerListLayoutManager);
        APIConnector.getInstance().listEngineers(new VolleyCallback() {
            final ArrayList<String> engineerList = new ArrayList<String>();

            @Override
            public void onSuccess(String engineer) {
                engineerList.add(engineer);
                engineerListAdapter = new IMRRecycleViewAdapter(engineerList);
                engineerListView.setAdapter(engineerListAdapter);

                engineerListAdapter.setOnItemClickListener(new IMRRecycleViewAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {

                        String itemText = engineerList.get(position);
                        //now that the text of the selected item is known, parse to get the ID
                        StringTokenizer tokeniser = new StringTokenizer(itemText, ";");
                        //First token is "Engineer ID: nnnn;" so substring after the ": "
                        selectedEngineerId = tokeniser.nextToken().substring(13);

                        if (!selectedJobId.isEmpty() && !selectedEngineerId.isEmpty()) {
                            assignEngineerButton.setEnabled(true);
                        }
                    }
                });
            }
        });
    }

    public void OnButtonClickedAssignEngineer(View view) throws JSONException {
        APIConnector.getInstance().assignEngineerToJob(new VolleyCallback() {
            @Override
            public void onSuccess(String result) {
                IMRSuccessDialog successDialog = new IMRSuccessDialog();
                successDialog.show(getSupportFragmentManager(),"");
            }
        }, selectedEngineerId, selectedJobId);
    }
}