package com.example.imrapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import org.json.JSONException;

public class CreateAreaActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_area);
    }

    public void onButtonClickedCreate(View view) {
        final TextView areaDescriptionTextView = findViewById(R.id.createAreaDescriptionText);
        String areaDescription = areaDescriptionTextView.getText().toString();

        if (!areaDescription.isEmpty()) {
            try {
                APIConnector.getInstance().createArea(new VolleyCallback() {
                    @Override
                    public void onSuccess(String areaId) {

                        if (areaId != "-1") {
                            IMRSuccessDialog successDialog = new IMRSuccessDialog();
                            successDialog.extraInfo = "Area ID: " + areaId;
                            successDialog.show(getSupportFragmentManager(), "");
                        }
                        else {
                            IMRFailureDialog failureDialog = new IMRFailureDialog();
                            failureDialog.show(getSupportFragmentManager(), "");
                        }
                    }
                }, areaDescription);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}