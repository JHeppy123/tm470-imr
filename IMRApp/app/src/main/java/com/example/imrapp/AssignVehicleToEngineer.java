package com.example.imrapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.StringTokenizer;

public class AssignVehicleToEngineer extends AppCompatActivity {

    private RecyclerView engineerListView;
    private IMRRecycleViewAdapter engineerListAdapter;
    private RecyclerView.LayoutManager engineerListLayoutManager;

    private RecyclerView vehicleListView;
    private IMRRecycleViewAdapter vehicleListAdapter;
    private RecyclerView.LayoutManager vehicleListLayoutManager;

    String selectedEngineerId = new String();
    String selectedVehicleId = new String();

    Button assignEngineerButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assign_vehicle_to_engineer);

        assignEngineerButton = (Button)findViewById(R.id.vehicleEngineerAssignButton);
        assignEngineerButton.setEnabled(false);

        engineerListView = (RecyclerView) findViewById(R.id.vehicleEngineerAssignEngineerList);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        engineerListView.setHasFixedSize(true);
        // use a linear layout manager
        engineerListLayoutManager = new LinearLayoutManager(this);
        engineerListView.setLayoutManager(engineerListLayoutManager);
        APIConnector.getInstance().listEngineers(new VolleyCallback() {
            final ArrayList<String> engineerList = new ArrayList<String>();

            @Override
            public void onSuccess(String engineer) {
                engineerList.add(engineer);
                engineerListAdapter = new IMRRecycleViewAdapter(engineerList);
                engineerListView.setAdapter(engineerListAdapter);

                engineerListAdapter.setOnItemClickListener(new IMRRecycleViewAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {

                        String itemText = engineerList.get(position);
                        //now that the text of the selected item is known, parse to get the ID
                        StringTokenizer tokeniser = new StringTokenizer(itemText, ";");
                        //First token is "Engineer ID: nnnn;" so substring after the ": "
                        selectedEngineerId = tokeniser.nextToken().substring(13);

                        if (!selectedVehicleId.isEmpty() && !selectedEngineerId.isEmpty()) {
                            assignEngineerButton.setEnabled(true);
                        }
                    }
                });
            }
        });

        vehicleListView = (RecyclerView) findViewById(R.id.vehicleEngineerAssignVehicleList);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        vehicleListView.setHasFixedSize(true);
        // use a linear layout manager
        vehicleListLayoutManager = new LinearLayoutManager(this);
        vehicleListView.setLayoutManager(vehicleListLayoutManager);

        APIConnector.getInstance().listVehicles(new VolleyCallback() {
            final ArrayList<String> vehicleList = new ArrayList<String>();

            @Override
            public void onSuccess(String vehicle) {
                vehicleList.add(vehicle);
                vehicleListAdapter = new IMRRecycleViewAdapter(vehicleList);
                vehicleListView.setAdapter(vehicleListAdapter);

                vehicleListAdapter.setOnItemClickListener(new IMRRecycleViewAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {

                        String itemText = vehicleList.get(position);
                        //now that the text of the selected item is known, parse to get the ID
                        StringTokenizer tokeniser = new StringTokenizer(itemText, ";");
                        selectedVehicleId = tokeniser.nextToken().substring(12);

                        if (!selectedVehicleId.isEmpty() && !selectedEngineerId.isEmpty()) {
                            assignEngineerButton.setEnabled(true);
                        }
                    }
                });
            }
        });
    }

    public void onButtonClickedAssign(View view) throws JSONException {
        APIConnector.getInstance().assignEngineerToVehicle(new VolleyCallback() {
            @Override
            public void onSuccess(String success) {
                if (success == "true") {
                    IMRSuccessDialog successDialog = new IMRSuccessDialog();
                    successDialog.show(getSupportFragmentManager(), "");
                }
                else {
                    IMRFailureDialog failureDialog = new IMRFailureDialog();
                    failureDialog.show(getSupportFragmentManager(), "");
                }
            }
        }, selectedEngineerId, selectedVehicleId);
    }
}