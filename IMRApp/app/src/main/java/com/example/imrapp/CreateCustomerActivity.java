package com.example.imrapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import org.json.JSONException;

import java.util.ArrayList;

public class CreateCustomerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_customer);
    }

    public void createCustomer(View view) {
        final TextView createdCustomerTextView = findViewById(R.id.createdCustomerEdit);
        final TextView customerNameTextView = findViewById(R.id.customerNameText);
        final TextView customerAddOneTextView = findViewById(R.id.customerAddOneText);
        final TextView customerAddTwoTextView = findViewById(R.id.customerAddTwoText);
        final TextView customerTownTextView = findViewById(R.id.customerTownText);
        final TextView customerCountyTextView = findViewById(R.id.customerCountyText);
        final TextView customerPostcodeTextView = findViewById(R.id.customerPostcodeText);
        final TextView customerTelTextView = findViewById(R.id.customerTelText);
        final TextView customerEmailTextView = findViewById(R.id.customerEmailText);

        try {
            APIConnector.getInstance().createCustomer(new VolleyCallback() {
                @Override
                public void onSuccess(String customerId) {
                    createdCustomerTextView.setText(customerId);
                    IMRSuccessDialog successDialog = new IMRSuccessDialog();
                    successDialog.show(getSupportFragmentManager(),"");
                }
            }, customerNameTextView.getText().toString(), customerAddOneTextView.getText().toString(), customerAddTwoTextView.getText().toString(),
                    customerTownTextView.getText().toString(), customerCountyTextView.getText().toString(), customerPostcodeTextView.getText().toString(),
                    customerTelTextView.getText().toString(), customerEmailTextView.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}