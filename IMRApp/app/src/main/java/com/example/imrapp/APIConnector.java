package com.example.imrapp;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class APIConnector {

    private static APIConnector instance;
    private Context context;
    private RequestQueue requestQueue;
    private String baseUrl = "http://192.168.0.10:3000/"; //NB: For testing, this matches the PC's IP address
    String requestResult = new String();

    private APIConnector() {
    }

    public static APIConnector getInstance() {
        if (instance == null) {
            instance = new APIConnector();
        }
        return instance;
    }

    public void init(Context context) {
        this.context = context;
        requestQueue = Volley.newRequestQueue(this.context);
    }

    /**
     * Lists all Jobs, returning the Job ID and the assigned customer ID to the supplied callback
     *
     * Format: "Job: nnn; Customer: sss" where nnn is the numerical ID and sss is a string
     *
     * @param callback Volley callback
     *
     *
     *
     */
    public void listJobs(final VolleyCallback callback) {

        //TODO: Add assigned engineer to the list if present
        String requestUrl = baseUrl + "job/list";

        JsonArrayRequest arrayRequest = new JsonArrayRequest(Request.Method.GET, requestUrl,
                null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                //parse the array
                for (int i = 0; i < response.length(); ++i) {
                    try {
                        JSONObject job = response.getJSONObject(i);
                        String jobId = job.getString("jobId");
                        String customerName = job.getString("customerName");
                        requestResult = "Job: " + jobId + "; Customer: " + customerName;
                        callback.onSuccess(requestResult);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onSuccess(error.toString());
            }
        });

        requestQueue.add(arrayRequest);
    }

    /**
     * Lists all Customers, returning the customer ID and name to the supplied callback
     *
     * Format: "Customer ID: nnn; Name: sss" where nnn is the numerical ID and sss is a string
     *
     * @param callback Volley callback
     *
     */
    public void listCustomers(final VolleyCallback callback) {
        String requestUrl = baseUrl + "customer/list";

        JsonArrayRequest arrayRequest = new JsonArrayRequest(Request.Method.GET, requestUrl, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                //parse the array
                for (int i = 0; i < response.length(); ++i) {
                    try {
                        JSONObject customer = response.getJSONObject(i);
                        String customerId = customer.getString("customerId");
                        String customerName = customer.getString("name");
                        requestResult = "Customer ID: " + customerId + "; Name: " + customerName;
                        callback.onSuccess(requestResult);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onSuccess(error.toString());
            }
        });

        requestQueue.add(arrayRequest);
    }

    /**
     * Lists all Engineers, returning the engineer ID and name to the supplied callback
     *
     * Format: "Engineer ID: nnn; Name: sss" where nnn is the numerical ID and sss is a string
     *
     * @param callback Volley callback
     *
     */
    public void listEngineers(final VolleyCallback callback) {
        String requestUrl = baseUrl + "user/engineer/list";

        JsonArrayRequest arrayRequest = new JsonArrayRequest(Request.Method.GET, requestUrl, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                //parse the array
                for (int i = 0; i < response.length(); ++i) {
                    try {
                        JSONObject engineer = response.getJSONObject(i);
                        String engineerId = engineer.getString("engineerId");
                        String engineerForename = engineer.getString("forename");
                        String engineerSurname = engineer.getString("surname");
                        requestResult = "Engineer ID: " + engineerId + "; Name: " + engineerForename + " " + engineerSurname;
                        callback.onSuccess(requestResult);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onSuccess(error.toString());
            }
        });

        requestQueue.add(arrayRequest);
    }

    /**
     * Adapted from https://kodejava.org/how-do-i-validate-email-address-using-regular-expression/
     * Accessed May 2020
     */
    private boolean emailIsValid(String email) {
        String regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        return email.matches(regex);
    }

    /**
     * Creates a customer in the database, returning the customerID
     *
     * @param callback Volley callback
     * @param customerName the customer's full name
     * @param customerAddressOne the first line of the address (e.g. building number)
     * @param customerAddressTwo the second line of the address
     * @param customerTown the address' town
     * @param customerCounty the address' county
     * @param customerPostcode the postcode
     * @param customerTel the telephone number, as a string
     * @param customerEmail the email, in the format 'string@string.domain'
     *
     */
    public void createCustomer(final VolleyCallback callback, String customerName, String customerAddressOne, String customerAddressTwo,
                               String customerTown, String customerCounty, String customerPostcode,
                               String customerTel, String customerEmail) throws JSONException {
        String requestUrl = baseUrl + "customer/create";

        assert callback != null;
        assert !customerName.isEmpty() && !customerAddressOne.isEmpty() && !customerAddressTwo.isEmpty()
                && !customerTown.isEmpty() && !customerCounty.isEmpty() && !customerPostcode.isEmpty()
                && !customerTel.isEmpty() && !customerEmail.isEmpty();
        assert emailIsValid(customerEmail);

        JSONObject customerObject = new JSONObject();
        customerObject.put("name", customerName);
        customerObject.put("addressOne", customerAddressOne);
        customerObject.put("addressTwo", customerAddressTwo);
        customerObject.put("addressTown", customerTown);
        customerObject.put("addressCounty", customerCounty);
        customerObject.put("addressPostcode", customerPostcode);
        customerObject.put("telephone", customerTel);
        customerObject.put("email", customerEmail);

        final String requestBody = customerObject.toString();

        JsonObjectRequest createCustomerRequest = new JsonObjectRequest
                (Request.Method.POST, requestUrl, customerObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String customerId = response.getString("customerId");
                            requestResult = "customerId: " + customerId;
                            callback.onSuccess(requestResult);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callback.onSuccess(error.toString());
                    }
                });

        requestQueue.add(createCustomerRequest);
    }

    /**
     * Creates an engineer in the database, returning the new engineer ID
     *
     * @param callback Volley callback
     * @param forename the engineer's first name
     * @param surname the engineer's surname
     */
    public void createEngineer(final VolleyCallback callback, String forename, String surname) throws JSONException {
        String requestUrl = baseUrl + "user/engineer/create";

        assert callback != null;
        assert !forename.isEmpty() && !surname.isEmpty();

        JSONObject engineerObject = new JSONObject();
        engineerObject.put("forename", forename);
        engineerObject.put("surname", surname);

        JsonObjectRequest createCustomerRequest = new JsonObjectRequest
                (Request.Method.POST, requestUrl, engineerObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String engineerId = response.getString("engineerId");
                            requestResult = "engineerId: " + engineerId;
                            callback.onSuccess(requestResult);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callback.onSuccess(error.toString());
                    }
                });

        requestQueue.add(createCustomerRequest);
    }

    /**
     * Creates a job in the database, returning the new job ID
     *
     * @param callback Volley callback
     * @param customerId the Customer to assign to the job
     * @param notes additional information about the job
     */
    public void createJob(final VolleyCallback callback, String customerId, String notes) throws JSONException {
        String requestUrl = baseUrl + "job/create";

        assert callback != null;
        assert !customerId.isEmpty();

        JSONObject jobObject = new JSONObject();
        jobObject.put("customerId", customerId);
        jobObject.put("jobTypeId", 1); //TODO: make this a variable
        jobObject.put("creatingUserId", 1); //TODO: same as above
        jobObject.put("startDate", "2020-08-02 00:00:00");
        jobObject.put("notes", notes);

        JsonObjectRequest createCustomerRequest = new JsonObjectRequest
                (Request.Method.POST, requestUrl, jobObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String jobId = response.getString("jobId");
                            requestResult = jobId;
                            callback.onSuccess(requestResult);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callback.onSuccess(error.toString());
                    }
                });

        requestQueue.add(createCustomerRequest);
    }

    /**
     * Assigns an engineer to a job
     *
     * Note, the job must be active for the engineer to be assigned successfully
     *
     * @param callback Volley callback
     * @param engineerId the Customer to assign to the job
     * @param jobId the job unique ID number
     */
    void assignEngineerToJob(final VolleyCallback callback, String engineerId, String jobId) throws JSONException {
        String requestUrl = baseUrl + "job/engineer/assign";

        assert callback != null;
        assert !jobId.isEmpty() && !engineerId.isEmpty();

        JSONObject jobEngineerAssignmentObject = new JSONObject();
        jobEngineerAssignmentObject.put("engineerId", engineerId);
        jobEngineerAssignmentObject.put("jobId", jobId);

        JsonObjectRequest assignEngineerRequest = new JsonObjectRequest(Request.Method.POST, requestUrl, jobEngineerAssignmentObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    String success = response.getString("success");
                    if (success == "true") {
                        Log.println(Log.INFO, "jobAssignment", "Success!");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onSuccess(error.toString());
            }
        });

        requestQueue.add(assignEngineerRequest);
    }

    /**
     * Sets the job to be flagged as management-complete, signalling the end of the job
     *
     * TODO: Needs a manager ID passed in
     *
     * @param callback Volley callback
     * @param jobId the job unique ID number
     */
    void managementCompleteJob(final VolleyCallback callback, String jobId) throws JSONException {
        String requestUrl = baseUrl + "job/manager/complete";

        assert callback != null;
        assert !jobId.isEmpty();

        JSONObject jobManagerCompleteObject = new JSONObject();
        jobManagerCompleteObject.put("managerId", "1"); //TODO: Needs a manager ID passed in
        jobManagerCompleteObject.put("jobId", jobId);

        JsonObjectRequest jobManagerCompleteRequest = new JsonObjectRequest(Request.Method.POST, requestUrl, jobManagerCompleteObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    String success = response.getString("success");
                    if (success == "true") {
                        callback.onSuccess("");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onSuccess(error.toString());
            }
        });

        requestQueue.add(jobManagerCompleteRequest);
    }

    /**
     * Creates a vehicle which can later be assigned to an engineer
     *
     * @param callback Volley callback
     * @param make vehicle make (10 char limit)
     * @param model vehicle model (10 char limit)
     * @param registration registration (10 char limit)
     */
    void createVehicle(final VolleyCallback callback, String make, String model, String registration) throws JSONException {
        String requestUrl = baseUrl + "vehicle/create";

        assert callback != null;
        assert !make.isEmpty() && !model.isEmpty() && !registration.isEmpty();

        JSONObject createVehicleObject = new JSONObject();
        createVehicleObject.put("vehicleMake", make);
        createVehicleObject.put("vehicleModel", model);
        createVehicleObject.put("vehicleRegistration", registration);

        JsonObjectRequest createVehicleRequest = new JsonObjectRequest(Request.Method.POST, requestUrl, createVehicleObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    String vehicleId = response.getString("vehicleId");
                    if (!vehicleId.isEmpty()) {
                        callback.onSuccess(vehicleId);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onSuccess(error.toString());
            }
        });

        requestQueue.add(createVehicleRequest);
    }

    /**
     * Returns a vehicle string to the supplied callback
     *
     * Format: "Vehicle ID: nnn; Reg.: sss; Make: sss; Model: sss"
     *
     * @param callback Volley callback
     */
    void listVehicles(final VolleyCallback callback) {
        String requestUrl = baseUrl + "vehicle/list";

        JsonArrayRequest arrayRequest = new JsonArrayRequest(Request.Method.GET, requestUrl, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                //parse the array
                for (int i = 0; i < response.length(); ++i) {
                    try {
                        JSONObject vehicle = response.getJSONObject(i);
                        String vehicleId = vehicle.getString("vehicleId");
                        String vehicleMake = vehicle.getString("vehicleMake");
                        String vehicleModel = vehicle.getString("vehicleModel");
                        String vehicleReg = vehicle.getString("vehicleRegistration");
                        requestResult = "Vehicle ID: " + vehicleId + "; Reg.: " + vehicleReg + "; Make: " + vehicleMake + "; Model: " + vehicleModel;
                        callback.onSuccess(requestResult);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onSuccess(error.toString());
            }
        });

        requestQueue.add(arrayRequest);
    }

    /**
     * Assigns the supplied engineer ID to the supplied vehicle ID
     *
     * @param callback the onsuccess callback
     * @param engineerId string
     * @param vehicleId string
     */
    void assignEngineerToVehicle(final VolleyCallback callback, String engineerId, String vehicleId) throws JSONException {
        String requestUrl = baseUrl + "vehicle/engineer/assign";

        JSONObject assignmentObject = new JSONObject();
        assignmentObject.put("engineerId", engineerId);
        assignmentObject.put("vehicleId", vehicleId);

        JsonObjectRequest assignmentRequest = new JsonObjectRequest(Request.Method.POST, requestUrl, assignmentObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    String success = response.getString("success");
                    callback.onSuccess(success);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onSuccess(error.toString());
            }
        });

        requestQueue.add(assignmentRequest);
    }


    void createArea(final VolleyCallback callback, String description) throws JSONException {
        String requestUrl = baseUrl + "area/create";

        JSONObject areaObject = new JSONObject();
        areaObject.put("areaDescription", description);

        JsonObjectRequest areaCreationRequest = new JsonObjectRequest(Request.Method.POST, requestUrl, areaObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    String areaId = response.getString("areaId");
                    callback.onSuccess(areaId);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onSuccess(error.toString());
            }
        });

        requestQueue.add(areaCreationRequest);
    }

    void listAreas(final VolleyCallback callback) {
        String requestUrl = baseUrl + "area/list";

        JsonArrayRequest arrayRequest = new JsonArrayRequest(Request.Method.GET, requestUrl, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                //parse the array
                for (int i = 0; i < response.length(); ++i) {
                    try {
                        JSONObject area = response.getJSONObject(i);
                        String areaId = area.getString("areaId");
                        String areaDescription = area.getString("areaDescription");
                        requestResult = "Area ID: " + areaId + "; Description: " + areaDescription;
                        callback.onSuccess(requestResult);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onSuccess(error.toString());
            }
        });

        requestQueue.add(arrayRequest);
    }


    void assignEngineerToArea(final VolleyCallback callback, String engineerId, String areaId) throws JSONException {
        String requestUrl = baseUrl + "area/engineer/assign";

        JSONObject assignmentObject = new JSONObject();
        assignmentObject.put("engineerId", engineerId);
        assignmentObject.put("areaId", areaId);

        JsonObjectRequest assignmentRequest = new JsonObjectRequest(Request.Method.POST, requestUrl, assignmentObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    String success = response.getString("success");
                    callback.onSuccess(success);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onSuccess(error.toString());
            }
        });

        requestQueue.add(assignmentRequest);
    }
}
