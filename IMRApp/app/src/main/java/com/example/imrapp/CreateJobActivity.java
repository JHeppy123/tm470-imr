package com.example.imrapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.StringTokenizer;

public class CreateJobActivity extends AppCompatActivity {

    private RecyclerView customerListView;
    private IMRRecycleViewAdapter customerListAdapter;
    private RecyclerView.LayoutManager customerListLayoutManager;

    private RecyclerView engineerListView;
    private IMRRecycleViewAdapter engineerListAdapter;
    private RecyclerView.LayoutManager engineerListLayoutManager;

    String selectedCustomerId;
    String selectedEngineerId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_job);

        customerListView = (RecyclerView) findViewById(R.id.customerListView);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        customerListView.setHasFixedSize(true);
        // use a linear layout manager
        customerListLayoutManager = new LinearLayoutManager(this);
        customerListView.setLayoutManager(customerListLayoutManager);

        APIConnector.getInstance().listCustomers(new VolleyCallback() {
            final ArrayList<String> customerList = new ArrayList<String>();

            @Override
            public void onSuccess(String customer) {
                customerList.add(customer);
                customerListAdapter = new IMRRecycleViewAdapter(customerList);
                customerListView.setAdapter(customerListAdapter);

                customerListAdapter.setOnItemClickListener(new IMRRecycleViewAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {

                        String itemText = customerList.get(position);
                        //now that the text of the selected item is known, parse to get the ID
                        StringTokenizer tokeniser = new StringTokenizer(itemText, ";");
                        //First token is "Customer ID: nnnn;" so substring after the ": "
                        selectedCustomerId = tokeniser.nextToken().substring(13);
                    }
                });

            }
        });

        engineerListView = (RecyclerView) findViewById(R.id.engineerListView);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        engineerListView.setHasFixedSize(true);
        // use a linear layout manager
        engineerListLayoutManager = new LinearLayoutManager(this);
        engineerListView.setLayoutManager(engineerListLayoutManager);

        APIConnector.getInstance().listEngineers(new VolleyCallback() {
            final ArrayList<String> engineerList = new ArrayList<String>();

            @Override
            public void onSuccess(String engineer) {
                engineerList.add(engineer);
                engineerListAdapter = new IMRRecycleViewAdapter(engineerList);
                engineerListView.setAdapter(engineerListAdapter);

                engineerListAdapter.setOnItemClickListener(new IMRRecycleViewAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {

                        String itemText = engineerList.get(position);
                        //now that the text of the selected item is known, parse to get the ID
                        StringTokenizer tokeniser = new StringTokenizer(itemText, ";");
                        //First token is "Engineer ID: nnnn;" so substring after the ": "
                        selectedEngineerId = tokeniser.nextToken().substring(13);
                    }
                });;
            }
        });
    }

    public void createJob(View view) {
        final TextView jobNotesTextView = findViewById(R.id.jobCreationNotesText);
        final TextView jobIdTextView = findViewById(R.id.createdJobIdTextView);

        try {
            APIConnector.getInstance().createJob(new VolleyCallback() {
                                                          @Override
                                                          public void onSuccess(String jobId) {
                                                              IMRSuccessDialog successDialog = new IMRSuccessDialog();
                                                              successDialog.show(getSupportFragmentManager(),"");
                                                              jobIdTextView.setText(jobId);

                                                              //If there's an engineer selected, automatically assign them to the new job
                                                              if (!selectedEngineerId.isEmpty()) {
                                                                  try {
                                                                      APIConnector.getInstance().assignEngineerToJob(new VolleyCallback() {
                                                                          @Override
                                                                          public void onSuccess(String result) {

                                                                          }
                                                                      }, selectedEngineerId, jobId);
                                                                  } catch (JSONException e) {
                                                                      e.printStackTrace();
                                                                  }

                                                              }
                                                          }
                                                      },
                    selectedCustomerId, jobNotesTextView.getText().toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}