package com.example.imrapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.StringTokenizer;

public class ListJobsActivity extends AppCompatActivity {

    private RecyclerView jobListView;
    private IMRRecycleViewAdapter jobListAdapter;
    private RecyclerView.LayoutManager layoutManager;
    Button assignEngineerButton;
    Button managementCompleteButton;

    String selectedJobId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_jobs);

        assignEngineerButton = (Button)findViewById(R.id.jobListAssignEngineerToJobButton);
        assignEngineerButton.setEnabled(false);

        managementCompleteButton = (Button)findViewById(R.id.listJobsManagementCompleteButton);
        managementCompleteButton.setEnabled(false);

        jobListView = (RecyclerView) findViewById(R.id.recyclerView);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        jobListView.setHasFixedSize(true);

        // use a linear layout manager
        layoutManager = new LinearLayoutManager(this);
        jobListView.setLayoutManager(layoutManager);

        APIConnector.getInstance().listJobs(new VolleyCallback() {
            final ArrayList<String> jobsList = new ArrayList<String>();

            @Override
            public void onSuccess(String job) {
                //TODO (JH): Not sure, but I think this will create a new container each time an
                // object is returned; i.e. if there are three objects, three containers will
                // be made, the first having one object, the second two, and the third three.
                // Creating the container two more times than necessary.  But currently works as-is
                jobsList.add(job);
                jobListAdapter = new IMRRecycleViewAdapter(jobsList);
                jobListView.setAdapter(jobListAdapter);

                jobListAdapter.setOnItemClickListener(new IMRRecycleViewAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {

                        String itemText = jobsList.get(position);
                        //now that the text of the selected item is known, parse to get the ID
                        StringTokenizer tokeniser = new StringTokenizer(itemText, ";");
                        //First token is "Job : nnnn;" so substring after the ": "
                        selectedJobId = tokeniser.nextToken().substring(5);

                        if (!selectedJobId.isEmpty()) {
                            assignEngineerButton.setEnabled(true);
                            managementCompleteButton.setEnabled(true);
                        }
                    }
                });
            }
        });
    }

    public void onButtonClickedAssignEngineer(View view) {
        Intent intent = new Intent(this, AssignEngineerToJob.class);
        intent.putExtra("selectedJobId", selectedJobId);
        startActivity(intent);
    }

    public void onButtonClickedManagementComplete(View view) throws JSONException {
        APIConnector.getInstance().managementCompleteJob(new VolleyCallback() {

            @Override
            public void onSuccess(String str) {
                IMRSuccessDialog successDialog = new IMRSuccessDialog();
                successDialog.show(getSupportFragmentManager(),"");
            }
        }, selectedJobId);
    }
}