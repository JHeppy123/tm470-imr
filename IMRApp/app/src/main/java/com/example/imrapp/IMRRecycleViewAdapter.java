package com.example.imrapp;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class IMRRecycleViewAdapter extends RecyclerView.Adapter<IMRRecycleViewAdapter.IMRViewHolder> {

    private ArrayList<String> dataset = new ArrayList<String>();

    public OnItemClickListener listener;
    // Define the listener interface
    public interface OnItemClickListener {
        void onItemClick(View itemView, int position);
    }
    // Define the method that allows the parent activity or fragment to define the listener
    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class IMRViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        // each data item is just a string in this case
        public TextView textView;
        public IMRViewHolder(TextView textView) {
            super(textView);
            this.textView = textView;
            textView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (listener != null) {
                int position = getAdapterPosition();
                if (position != RecyclerView.NO_POSITION) {
                    listener.onItemClick(itemView, position);
                }
            }
        }
    }

    public IMRRecycleViewAdapter(ArrayList<String> stringList) {
        dataset = stringList;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public IMRViewHolder onCreateViewHolder(ViewGroup parent,
                                            int viewType) {
        TextView layoutTextView = new TextView(parent.getContext());
        IMRViewHolder layoutViewHolder = new IMRViewHolder(layoutTextView);
        return layoutViewHolder;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(IMRViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.textView.setText(dataset.get(position));
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        //return dataset.length;
        return dataset.size();
    }
}