package com.example.imrapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import org.json.JSONException;

public class CreateVehicleActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_vehicle);
    }

    public void onButtonClickedCreateVehicle(View view) throws JSONException {

        final TextView makeTextView = findViewById(R.id.createVehicleMakeText);
        final TextView modelTextView = findViewById(R.id.createVehicleModelText);
        final TextView registrationTextView = findViewById(R.id.createVehicleRegistrationText);

        String make = makeTextView.getText().toString();
        String model = modelTextView.getText().toString();
        String registration = registrationTextView.getText().toString();

        if (make.isEmpty() || model.isEmpty() || registration.isEmpty()) {
            IMRFailureDialog failureDialog = new IMRFailureDialog();
            failureDialog.extraInfo = "One or more fields were empty";
            failureDialog.show(getSupportFragmentManager(), "");
            return;
        }

        APIConnector.getInstance().createVehicle(new VolleyCallback() {
            @Override
            public void onSuccess(String vehicleId) {
                IMRSuccessDialog successDialog = new IMRSuccessDialog();
                successDialog.extraInfo = "Vehicle ID: " + vehicleId;
                successDialog.show(getSupportFragmentManager(), vehicleId);
            }
        }, make, model, registration);
    }
}