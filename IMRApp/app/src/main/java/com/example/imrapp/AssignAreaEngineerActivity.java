package com.example.imrapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.StringTokenizer;

public class AssignAreaEngineerActivity extends AppCompatActivity {

    private RecyclerView engineerListView;
    private IMRRecycleViewAdapter engineerListAdapter;
    private RecyclerView.LayoutManager engineerListLayoutManager;
    private RecyclerView areaListView;
    private IMRRecycleViewAdapter areaListAdapter;
    private RecyclerView.LayoutManager areaListLayoutManager;

    String selectedEngineerId = new String();
    String selectedAreaId = new String();

    Button assignEngineerButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assign_area_engineer);

        assignEngineerButton = (Button)findViewById(R.id.assignAreaEngineerButton);
        assignEngineerButton.setEnabled(false);

        engineerListView = (RecyclerView) findViewById(R.id.assignAreaEngineerList);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        engineerListView.setHasFixedSize(true);
        // use a linear layout manager
        engineerListLayoutManager = new LinearLayoutManager(this);
        engineerListView.setLayoutManager(engineerListLayoutManager);
        APIConnector.getInstance().listEngineers(new VolleyCallback() {
            final ArrayList<String> engineerList = new ArrayList<String>();

            @Override
            public void onSuccess(String engineer) {
                engineerList.add(engineer);
                engineerListAdapter = new IMRRecycleViewAdapter(engineerList);
                engineerListView.setAdapter(engineerListAdapter);

                engineerListAdapter.setOnItemClickListener(new IMRRecycleViewAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {

                        String itemText = engineerList.get(position);
                        //now that the text of the selected item is known, parse to get the ID
                        StringTokenizer tokeniser = new StringTokenizer(itemText, ";");
                        //First token is "Engineer ID: nnnn;" so substring after the ": "
                        selectedEngineerId = tokeniser.nextToken().substring(13);

                        if (!selectedAreaId.isEmpty() && !selectedEngineerId.isEmpty()) {
                            assignEngineerButton.setEnabled(true);
                        }
                    }
                });
            }
        });

        areaListView = (RecyclerView) findViewById(R.id.assignAreaAreaList);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        areaListView.setHasFixedSize(true);
        // use a linear layout manager
        areaListLayoutManager = new LinearLayoutManager(this);
        areaListView.setLayoutManager(areaListLayoutManager);
        APIConnector.getInstance().listAreas(new VolleyCallback() {
            final ArrayList<String> areaList = new ArrayList<String>();

            @Override
            public void onSuccess(String area) {
                areaList.add(area);
                areaListAdapter = new IMRRecycleViewAdapter(areaList);
                areaListView.setAdapter(areaListAdapter);

                areaListAdapter.setOnItemClickListener(new IMRRecycleViewAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {

                        String itemText = areaList.get(position);
                        //now that the text of the selected item is known, parse to get the ID
                        StringTokenizer tokeniser = new StringTokenizer(itemText, ";");
                        selectedAreaId = tokeniser.nextToken().substring(9);

                        if (!selectedAreaId.isEmpty() && !selectedEngineerId.isEmpty()) {
                            assignEngineerButton.setEnabled(true);
                        }
                    }
                });
            }
        });
    }

    public void onButtonClickedAssign(View view) throws JSONException {
        APIConnector.getInstance().assignEngineerToArea(new VolleyCallback() {
            @Override
            public void onSuccess(String result) {
                IMRSuccessDialog successDialog = new IMRSuccessDialog();
                successDialog.show(getSupportFragmentManager(),"");
            }
        }, selectedEngineerId, selectedAreaId);
    }
}