package com.example.imrapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

interface VolleyCallback {
    void onSuccess(String result);
}

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //instantiate the APIConnector singleton here
        APIConnector.getInstance().init(this);
    }

    public void createJobListActivity(View view) {

        Intent intent = new Intent(this, ListJobsActivity.class);
        startActivity(intent);
    }

    public void createCreateCustomerActivity(View view) {
        Intent intent = new Intent(this, CreateCustomerActivity.class);
        startActivity(intent);
    }

    public void createCreateEngineerActivity(View view) {
        Intent intent = new Intent(this, CreateEngineerActivity.class);
        startActivity(intent);
    }

    public void createJobActivity(View view) {
        Intent intent = new Intent(this, CreateJobActivity.class);
        startActivity(intent);
    }

    public void createVehicleActivity(View view) {
        Intent intent = new Intent(this, CreateVehicleActivity.class);
        startActivity(intent);
    }

    public void assignVehicleToEngineer(View view) {
        Intent intent = new Intent(this, AssignVehicleToEngineer.class);
        startActivity(intent);
    }

    public void createArea(View view) {
        Intent intent = new Intent(this, CreateAreaActivity.class);
        startActivity(intent);
    }

    public void assignAreaToEngineer(View view) {
        Intent intent = new Intent(this, AssignAreaEngineerActivity.class);
        startActivity(intent);
    }
}