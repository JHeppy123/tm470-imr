package com.example.imrapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import org.json.JSONException;

public class CreateEngineerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_engineer);
    }

    public void createEngineer(View view) {
        final TextView createdEngineerTextView = findViewById(R.id.engineerIDText);
        final TextView engineerForenameTextView = findViewById(R.id.engineerForenameText);
        final TextView engineerSurnameTextView = findViewById(R.id.engineerSurnameText);

        try {
            APIConnector.getInstance().createEngineer(new VolleyCallback() {
                  @Override
                  public void onSuccess(String engineerId) {
                      createdEngineerTextView.setText(engineerId);
                      IMRSuccessDialog successDialog = new IMRSuccessDialog();
                      successDialog.show(getSupportFragmentManager(),"");
                  }
              },
            engineerForenameTextView.getText().toString(), engineerSurnameTextView.getText().toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}