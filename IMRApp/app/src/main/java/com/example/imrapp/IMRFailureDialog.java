package com.example.imrapp;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;

public class IMRFailureDialog extends DialogFragment {

    public String extraInfo = new String("Generic");

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        String errMsg = new String(getResources().getString(R.string.failureDialog));

        if (!extraInfo.isEmpty()) {
            errMsg += " - " + extraInfo;
        }

        builder.setMessage(errMsg)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        onDestroy();
                    }
                });
        // Create the AlertDialog object and return it
        return builder.create();
    }
}
